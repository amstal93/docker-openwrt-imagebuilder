FROM alpine:latest

LABEL \
maintainer="Marek Rychly <marek.rychly@gmail.com>"

ARG OPENWRT_ARCH
ARG OPENWRT_HOST
ARG OPENWRT_TARGET
ARG OPENWRT_SUBTARGET
ARG OPENWRT_VERSION

ENV \
OPENWRT_ARCH="${OPENWRT_ARCH:-mips_24kc}" \
OPENWRT_HOST="${OPENWRT_HOST:-Linux-x86_64}" \
OPENWRT_TARGET="${OPENWRT_TARGET:-ath79}" \
OPENWRT_SUBTARGET="${OPENWRT_SUBTARGET:-generic}" \
OPENWRT_VERSION="${OPENWRT_VERSION:-21.02.0}"

ENV \
IMAGEBUILDER_NAME="openwrt-imagebuilder-${OPENWRT_VERSION}-${OPENWRT_TARGET}-${OPENWRT_SUBTARGET}.${OPENWRT_HOST}"

ENV \
IMAGEBUILDER_URL="https://downloads.openwrt.org/releases/${OPENWRT_VERSION}/targets/${OPENWRT_TARGET}/${OPENWRT_SUBTARGET}/${IMAGEBUILDER_NAME}.tar.xz" \
IMAGEBUILDER_HOME="/opt/${IMAGEBUILDER_NAME}"

RUN true \
# download and enter Image Builder (both Python2 and Python3 required)
&& apk --no-cache add make bash gawk perl python2 python3 file bzip2 gcc g++ musl-dev ncurses-dev git wget rsync \
&& echo "Downloading ${IMAGEBUILDER_URL} ..." && mkdir -p "${IMAGEBUILDER_HOME%/*}" && wget -qO - "${IMAGEBUILDER_URL}" | tar xJ -C "${IMAGEBUILDER_HOME%/*}" && echo "OpenWrt Image Builder downloaded and extracted" \
&& cd "${IMAGEBUILDER_HOME}" \
# need not to use update/install packages from feeds as they are already known and their pre-built *.ipk packages will be downloaded automatically
#&& wget https://downloads.openwrt.org/releases/${OPENWRT_VERSION}/packages/${OPENWRT_ARCH}/feeds.conf \
#&& mkdir -p ./package && ./scripts/feeds update -a && ./scripts/feeds install -a \
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*
